from django.shortcuts import render, get_object_or_404
from django.contrib.auth.models import User
from django.contrib.auth.mixins import LoginRequiredMixin,\
    UserPassesTestMixin
from django.views.generic import ListView, DetailView, CreateView,\
    UpdateView, DeleteView

from .models import Post


class PostListView(ListView):
    model = Post
    template_name = 'blog/home.html'
    context_object_name = 'posts'
    ordering = ['-date_posted']
    paginate_by = 8


class UserPostListView(ListView):
    """Get all the User posts"""
    model = Post
    template_name = 'blog/user_posts.html'
    context_object_name = 'posts'
    paginate_by = 8

    def get_queryset(self):
        # get the user
        user = get_object_or_404(User, username=self.kwargs.get('username'))
        return Post.objects.filter(author=user).order_by('date_posted')


class PostDetailView(DetailView):
    model = Post
    context_object_name = 'post'


class PostCreateView(LoginRequiredMixin, CreateView):
    model = Post
    fields = ['title', 'content']
    success_url = '/'
    
    def form_valid(self, form):
        form.instance.author = self.request.user
        return super(PostCreateView, self).form_valid(form)


class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Post
    fields = ['title', 'content']
    success_url = '/'

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super(PostUpdateView, self).form_valid(form)

    def test_func(self):
        """
        check if user is author return True,
        otherwise return false | 403 error
        """
        post = self.get_object()  # get the post
        if self.request.user == post.author:  # check if user is author
            return True  # let user update post
        return False


class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Post
    success_url = '/'

    def test_func(self):
        """
        check if user is author return True,
        otherwise return false | 403 error
        """
        post = self.get_object()  # get the post
        if self.request.user == post.author:  # check if user is author
            return True  # let user delete post
        return False


def about(request):
    return render(request, 'blog/about.html')
